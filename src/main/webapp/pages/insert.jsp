<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="Controller">
		<c:if test="${user == null}">
			<input type="text" name="action" value="insert_user" hidden="true" />
		</c:if>
		<c:if test="${user != null}">
			<input type="text" name="action" value="update_user" hidden="true" />
		</c:if>
		<input type="text" name="id" value="${user.id}" hidden="true" />
		<b>UserName:</b><input type="text" name="username" value="${user.username}" />
		<b>Password:</b><input type="text" name="password" value="${user.password}" />
		<input type="submit" value="Register">
	</form>
</body>
</html>