package by.bsuir.users.datasource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

public class Database {

    private static final String PROPERTIES_PATH = "by.bsuir.users.resources.database";
    private static ResourceBundle resource;

    static {
        resource = ResourceBundle.getBundle(PROPERTIES_PATH);
        
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException ex) {
            throw new ExceptionInInitializerError();
        }
    }

    public static Connection getConnection() {
        Connection db;
        try {            
            Properties properties = new Properties();
            properties.setProperty("user", resource.getString("username"));
            properties.setProperty("password", resource.getString("password"));
            properties.setProperty("useUnicode", resource.getString("useUnicode"));
            properties.setProperty("characterEncoding", resource.getString("encoding"));
            
            db = DriverManager.getConnection(resource.getString("uri"), properties);
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getSQLState() + ex.getMessage());
        }
        return db;
    }
}
