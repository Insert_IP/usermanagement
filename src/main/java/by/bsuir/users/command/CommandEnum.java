package by.bsuir.users.command;

import by.bsuir.users.command.impl.CreateCommand;
import by.bsuir.users.command.impl.DeleteCommand;
import by.bsuir.users.command.impl.ReadCommand;
import by.bsuir.users.command.impl.RedirectInsertCommand;
import by.bsuir.users.command.impl.RedirectUpdateCommand;
import by.bsuir.users.command.impl.SwitchColorCommand;
import by.bsuir.users.command.impl.UpdateCommand;

public enum CommandEnum {  //список комманд

    INSERT_USER {
                {
                    this.command = new CreateCommand();
                }
            },
    READ_USERS {
                {
                    this.command = new ReadCommand();
                }
            },
    UPDATE_USER {
                {
                    this.command = new UpdateCommand();
                }
            },
    DELETE_USER {
                {
                    this.command = new DeleteCommand();
                }
            },
    VIEW_UPDATE {  //редирект на страницу
                {
                    this.command = new RedirectUpdateCommand();
                }
            },
    VIEW_INSERT {  //редирект на страницу
                {
                    this.command = new RedirectInsertCommand();
                }
            },
    CHANGE_COLOR {
                {
                    this.command = new SwitchColorCommand();
                }
            };
    Command command;

    public Command getCommand() {
        return command;
    }
}
