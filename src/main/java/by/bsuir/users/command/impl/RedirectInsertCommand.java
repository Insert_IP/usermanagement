/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.bsuir.users.command.impl;

import by.bsuir.users.command.Command;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author bit
 */
public class RedirectInsertCommand implements Command{

    @Override
    public String execute(HttpServletRequest request) {
        return INSERT_PAGE;
    }
    
}
