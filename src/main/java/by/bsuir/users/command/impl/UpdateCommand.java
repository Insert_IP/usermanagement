package by.bsuir.users.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.users.command.Command;
import by.bsuir.users.dao.UserDAO;
import by.bsuir.users.dao.impl.UserDAOImpl;
import by.bsuir.users.entity.User;

public class UpdateCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		UserDAO uDao = new UserDAOImpl();
		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		
		uDao.update(user);
		
		return VIEW_LIST;
	}

}
