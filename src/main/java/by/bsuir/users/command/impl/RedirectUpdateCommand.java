package by.bsuir.users.command.impl;

import by.bsuir.users.command.Command;
import by.bsuir.users.entity.User;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RedirectUpdateCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int id = Integer.parseInt(request.getParameter("id"));  //берем запрошенный идентификатор
        ArrayList<User> list = (ArrayList<User>) session.getAttribute("list");  //берем из сессии список юзеров
        
        for(User usr : list){  //пока список не закончился...
            if(usr.getId() == id){  //ищем того, у кого тот идентификатор, который просит показать пользователь
                request.setAttribute("user", usr);  //отправляем его как ответ на запрос
            }
        }
        
        return INSERT_PAGE;
    }

}
