package by.bsuir.users.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.users.command.Command;
import by.bsuir.users.dao.UserDAO;
import by.bsuir.users.dao.impl.UserDAOImpl;
import by.bsuir.users.entity.User;
import java.time.LocalDate;

public class CreateCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		UserDAO uDao = new UserDAOImpl();  //инициализировали БД слой
		
		User user = new User();  //строим объект для вставки в БД
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
                user.setRegistered(LocalDate.now());
		
		uDao.create(user);  //собственно вставка
		
		return VIEW_LIST;  //отображаем ее на страничке view.jsp
	}
	
}
