package by.bsuir.users.command.impl;

import by.bsuir.users.command.Command;
import javax.servlet.http.HttpServletRequest;

public class SwitchColorCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("color", request.getParameter("color"));
        return VIEW_LIST;
    }
    
}
