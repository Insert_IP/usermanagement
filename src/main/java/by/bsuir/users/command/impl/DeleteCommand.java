package by.bsuir.users.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.users.command.Command;
import by.bsuir.users.dao.UserDAO;
import by.bsuir.users.dao.impl.UserDAOImpl;

public class DeleteCommand implements Command {
	@Override
	public String execute(HttpServletRequest request) {
		UserDAO uDao = new UserDAOImpl();
		
		uDao.delete(Integer.parseInt(request.getParameter("id")));
		
		return VIEW_LIST;
	}
}
