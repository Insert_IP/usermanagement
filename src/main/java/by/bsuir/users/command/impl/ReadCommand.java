package by.bsuir.users.command.impl;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.users.command.Command;
import by.bsuir.users.dao.UserDAO;
import by.bsuir.users.dao.impl.UserDAOImpl;
import by.bsuir.users.entity.User;
import javax.servlet.http.HttpSession;

public class ReadCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		UserDAO uDao = new UserDAOImpl();
		ArrayList<User> list =  uDao.read();  //получили список пользователей
                HttpSession session = request.getSession();  
                session.setAttribute("list", list);  //кинули его в сессию
		request.setAttribute("list", list);  //кинули его в ответ на запрос
		return INDEX_PAGE;
	}

}
