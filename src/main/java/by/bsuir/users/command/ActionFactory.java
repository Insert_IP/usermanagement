package by.bsuir.users.command;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {
    public Command defineCommand(HttpServletRequest request){
        String action = request.getParameter("action");  //достали параметр из запроса
        CommandEnum currentCommand = CommandEnum.valueOf(action.toUpperCase().trim());  //определили комманду из перечисления
        Command command = currentCommand.getCommand();  //достали из константы
        return command;  //вернули
    }
}
