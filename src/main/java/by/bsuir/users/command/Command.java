package by.bsuir.users.command;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    
    String INDEX_PAGE = "/pages/view.jsp";
    String INSERT_PAGE = "/pages/insert.jsp";
    String VIEW_LIST = "/Controller?action=read_users";
    
    String execute(HttpServletRequest request);
}
