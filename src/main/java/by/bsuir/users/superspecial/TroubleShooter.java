package by.bsuir.users.superspecial;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class TroubleShooter {

    private static ArrayList<File> getFiles() throws IOException {
        ArrayList<File> list = new ArrayList<>();

        int i = 0;
        Files.walk(Paths.get("target/users")).forEach(filePath -> {
                list.add(new File(filePath.toString()));
                System.out.println(filePath);
        });
        
        return list;
    }

    private static void archiveToJAR(ArrayList<File> list) {
        try {
            byte buffer[] = new byte[10240];
            FileOutputStream stream = new FileOutputStream("executable.jar");
            JarOutputStream out = new JarOutputStream(stream, new Manifest());

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) == null || !list.get(i).exists()
                        || list.get(i).isDirectory()) {
                    continue;
                }
                System.out.println("Adding " + list.get(i).getName());

                JarEntry jarAdd = new JarEntry(list.get(i).getName());
                jarAdd.setTime(list.get(i).lastModified());
                out.putNextEntry(jarAdd);

                FileInputStream in = new FileInputStream(list.get(i));
                while (true) {
                    int nRead = in.read(buffer, 0, buffer.length);
                    if (nRead <= 0) {
                        break;
                    }
                    out.write(buffer, 0, nRead);
                }
                in.close();
            }

            out.close();
            stream.close();
            System.out.println("Adding completed OK");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        try {
            ArrayList<File> list = getFiles();
            for (File f : list) {
                f.getName();
            }
            archiveToJAR(list);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
