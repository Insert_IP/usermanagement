package by.bsuir.users.dao;

import java.util.ArrayList;

import by.bsuir.users.entity.User;

public interface UserDAO {
    
	boolean create(User user);
        
	ArrayList<User> read();
        
	boolean update(User user);
        
	boolean delete(int id);
}
