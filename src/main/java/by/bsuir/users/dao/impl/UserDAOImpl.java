package by.bsuir.users.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import by.bsuir.users.dao.UserDAO;
import by.bsuir.users.datasource.Database;
import by.bsuir.users.entity.User;

public class UserDAOImpl implements UserDAO {
	
	private final String INSERT_USER = "INSERT INTO USERS(USR_USERNAME, USR_PASSWORD, USR_REGISTERED) VALUES(?, ?, ?)";
	private final String FETCH_USERS = "SELECT USR_USER_ID, USR_USERNAME, USR_PASSWORD, USR_REGISTERED FROM USERS";
	private final String UPDATE_USER = "UPDATE USERS SET USR_USERNAME = ?, USR_PASSWORD = ? WHERE USR_USER_ID = ?";
	private final String DELETE_USER = "DELETE FROM USERS WHERE USR_USER_ID = ?";
	
        @Override
	public boolean create(User user) {  //сохранить юзера
		boolean status = false;  //статус процесса
		Connection con = Database.getConnection();  //получили соединение
		try(PreparedStatement ps = con.prepareStatement(INSERT_USER)){  //отправили запрос
			ps.setString(1, user.getUsername());  //ставим вместо вопросиков параметры
			ps.setString(2, user.getPassword());  //тоже самое
			ps.setTimestamp(3, Timestamp.valueOf(user.getRegistered().atStartOfDay()));  //тоже самое
			ps.executeUpdate();  //выполнить запрос
			
			status = true;  //усе атлижна
		} catch(SQLException e){  //а если накрылось...
			e.printStackTrace();  //напишем причину
		} finally{  //но при любых условиях...
                    try {
                        con.close();  //закроем соединение
                    } catch (SQLException ex) {  //а если не закроется...
                        ex.printStackTrace();  //напишем причину
                    }
                }
		return status;
	}

        @Override
	public ArrayList<User> read() {  //читаем вех юзеров
		ArrayList<User> users = null;  //список юзеров
		User user;
		Connection con = Database.getConnection();
		try(PreparedStatement ps = con.prepareStatement(FETCH_USERS)){
			ResultSet rs = ps.executeQuery();
                        
			users = new ArrayList<>();  //инициализация списка
			while(rs.next()){  //пока кортежи не закончаться
                                user = new User();  //делаем объекты юзеров...
				user.setId(rs.getInt(1));
				user.setUsername(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setRegistered(rs.getTimestamp(4).toLocalDateTime().toLocalDate());
				
				users.add(user);  //и сохраняем их в список
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
		return users;
	}

        @Override
	public boolean update(User user) {  //обновить пользователя
		boolean status = false;
		Connection con = Database.getConnection();
		try(PreparedStatement ps = con.prepareStatement(UPDATE_USER)){
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setInt(3, user.getId());
			ps.executeUpdate();
			
			status = true;
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
		return status;
	}

        @Override
	public boolean delete(int id) {  //удалить
		boolean status = false;
		Connection con = Database.getConnection();
		try(PreparedStatement ps = con.prepareStatement(DELETE_USER)){
			ps.setInt(1, id);
			ps.executeUpdate();
			
			status = true;
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
		return status;
	}
	
}
