<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main Page</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>User ID</th>
                <th>Username</th>
                <th>Registered</th>
            </tr>
            <c:forEach items="${list}" var="user">
                <tr>
                    <td style="color: ${color}">${user.id}</td>
                    <td style="color: ${color}">${user.username}</td>
                    <td style="color: ${color}">${user.registered}</td>
                    <td><a href="/users/Controller?action=view_update&id=${user.id}">Update</a></td>
                    <td><a href="/users/Controller?action=delete_user&id=${user.id}">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
        <input type="button" onclick="location.href = '/users/Controller?action=view_insert'" size="100%" value="Insert User" />
        
        <form action="Controller">
            <input type="text" name="action" value="change_color" hidden />
            <input type="text" name="color" />
            <input type="submit" value="Switch Color" />
        </form>
    </body>
</html>
